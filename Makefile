## Source fastjet first!!

CXX = g++
CXXFLAGS += -Wall -O3 -fPIC -std=c++11 -pedantic
CXXFLAGS += -Iinclude -DCHECKING
CXXFLAGS += -I/mt/batch/jaraz/3.0.3-neworders/HEPTools/hepmc/include
LIBFLAGS += -L/mt/batch/jaraz/3.0.3-neworders/HEPTools/lib -lHepMC

CXXFLAGS_HOME += -Wall -O3 -fPIC -std=c++11 -pedantic
CXXFLAGS_HOME += -Iinclude -DCHECKING
CXXFLAGS_HOME += -I/usr/local/include/HepMC
LIBFLAGS_HOME += -L/usr/local/lib -lHepMC

SOURCES= hepmcreduce.cc
OBJECTS=$(SOURCES:.cc=.o)

all:
	make ippp

ippp: $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) $(LIBFLAGS) -o hepmcreduce

home: $(OBJECTS)
	$(CXX) $(CXXFLAGS_HOME) $(OBJECTS) $(LIBFLAGS_HOME) -o hepmcreduce

clean:
	rm -f $(OBJECTS) hepmcreduce
