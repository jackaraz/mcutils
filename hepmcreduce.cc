// -*- C++ -*-
//
// This file is part of MCUtils -- https://bitbucket.org/andybuckley/mcutils
// Copyright (C) 2013-2014 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//

#include <iomanip>

#include "HepMC/IO_GenEvent.h"
#include "MCUtils/HepMCUtils.h"

using namespace std;

/// @file Demo of filtering a HepMC record
/// @author Andy Buckley <andy.buckley@cern.ch>
//
// NOTE: Requires HepMC. Compile with e.g.
//  g++ -o hepmcreduce hepmcreduce.cc -Iinclude -I/path/to/hep/installs/include -L/path/to/hep/installs/lib -lHepMC

// A classifier function for GenParticles we want to remove
bool find_bad_particles(const HepMC::GenParticle* p)
{
    // Remove "null" particles used for HEPEVT padding
    if (p->pdg_id() == 0)
        return true;
    // Always keep EW particles, BSM particles, and heavy flavour partons & hadrons
    /// TODO Also keep particles directly connected to EW & BSM objects?
    /// TODO Intermediate replica removal?
    if (MCUtils::isResonance(p))
        return false;
    if (MCUtils::isBSM(p))
        return false;
    if (MCUtils::isHeavyFlavour(p))
        return false;

    // Keep top quarks
    if (MCUtils::isTop(p))
        return false;

    // Keep tau
    if (MCUtils::isTau(p))
        return false;

    // Kill (simple) loops
    // if (p->production_vertex() == p->end_vertex() && p->end_vertex() != NULL) return true;

    // Remove partons (other than those already preserved)
    if (MCUtils::isParton(p))
        return true;

    // Remove non-standard particles
    // if (MCUtils::isGenSpecific(p)) return true;
    // if (MCUtils::isDiquark(p)) return true;

    return false;
}

int main(int argc, char** argv)
{
    // Configure input event file from command line
    string infile = "in.hepmc";
    if (argc > 1)
        infile = argv[1];
    HepMC::IO_GenEvent in(infile, ios::in);

    // Configure output event file from command line
    string outfile = infile.substr(0, infile.rfind(".")) + "-reduced.hepmc";
    if (argc > 2)
        outfile = argv[2];
    HepMC::IO_GenEvent out(outfile, ios::out);

    double initial_npart = 0.;
    double initial_nvertex = 0.;
    double final_npart = 0.;
    double final_nvertex = 0.;

    // Event loop
    int nevt = 0;
    HepMC::GenEvent* ge = in.read_next_event();
    while (ge) 
    {
        nevt += 1;

        // Event properties before filtering
        const int particles_size_orig = ge->particles_size();
        const int vertices_size_orig = ge->vertices_size();
        initial_npart += particles_size_orig;
        initial_nvertex += vertices_size_orig;
#ifdef CHECKING
        const int num_orphan_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
        const int num_noparent_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
        const int num_nochild_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
#endif

        // Consistently remove the unwanted particles from the event
        MCUtils::reduce(ge, find_bad_particles);

        // Event properties after filtering
        const int particles_size_filt = ge->particles_size();
        const int vertices_size_filt = ge->vertices_size();
        final_npart += particles_size_filt;
        final_nvertex += vertices_size_filt;
#ifdef CHECKING
        const int num_orphan_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
        const int num_noparent_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
        const int num_nochild_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
#endif

        // Write out the change in the number of particles, etc.
        if (nevt % 100 == 0 || nevt == 1) 
        {
            double particle_reduction_rate = 100. * double(final_npart) / double(initial_npart);
            double vertex_reduction_rate = 100. * double(final_nvertex) / double(initial_nvertex);
            cout << "#" << setw(6) << nevt << ": Particle reduction " << setprecision(3)
                 << particle_reduction_rate << "%, Vertex reduction " << setprecision(3)
                 << vertex_reduction_rate << "% " << endl;
#ifdef CHECKING
            if (num_orphan_vtxs_filt != num_orphan_vtxs_orig)
                cerr << "WARNING! Change in num orphaned vertices: "
                     << num_orphan_vtxs_orig << " -> " << num_orphan_vtxs_filt << endl;
            if (num_noparent_vtxs_filt != num_noparent_vtxs_orig)
                cerr << "WARNING! Change in num no-parent vertices: "
                     << num_noparent_vtxs_orig << " -> " << num_noparent_vtxs_filt << endl;
            if (num_nochild_vtxs_filt != num_nochild_vtxs_orig)
                cerr << "WARNING! Change in num no-child vertices: "
                     << num_nochild_vtxs_orig << " -> " << num_nochild_vtxs_filt << endl;
#endif
        }
        // Write out reduced event
        out << ge;
        delete ge;
        in >> ge;
    }
    return 0;
}
